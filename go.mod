module gitlab.com/ferrysourcing/ferrybroker.git

go 1.16

require (
	github.com/google/uuid v1.1.2
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/grpc v1.45.0
	google.golang.org/protobuf v1.28.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
