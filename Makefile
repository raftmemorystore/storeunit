make:
	go build ./cmd/ferrybroker

proto:
	protoc --go_out=./pkg --go_opt=paths=source_relative \
    --go-grpc_out=./pkg --go-grpc_opt=paths=source_relative \
    api/raft/RaftNode.proto

lint:
	golangci-lint run ./...

clean:
	rm ferrybroker

docker:
	docker build -t ferrybroker:v0.0.1 -f deployment/ferrybroker/Dockerfile .
	docker build -t proxyunit:v0.0.1 -f deployment/proxyunit/Dockerfile .

shell:
	kubectl run my-shell --rm -i --tty --image tutum/dnsutils -- bash