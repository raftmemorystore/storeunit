package grpcservercontroller

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/ferrysourcing/ferrybroker.git/pkg/api/raft"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func (ns *GRPCNodeServerImpl) dicoverNodes() {
	for _, nodeURL := range ns.nodeURLs {
		if _, ok := ns.raftNodesGUIDSbyURL[nodeURL]; !ok {
			log.Debugln("Identifying ", nodeURL)
			conn, err := grpc.Dial(nodeURL, grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				log.Errorf(":( can not connect to %s %s", nodeURL, err.Error())
				continue
			}
			client := raft.NewRaftNodeClient(conn)
			whoIsResp, err := client.WhoIsWho(context.Background(), &raft.WhoIs{
				NodeGUID: ns.nodeID,
			})
			if err != nil {
				log.Errorf(":( can not identify %s %s", nodeURL, err.Error())
				defer conn.Close()
				continue
			}
			ns.raftNodesGUIDSbyURL[nodeURL] = whoIsResp.NodeGUID
			ns.grpcNodeClientByGUID.Store(whoIsResp.NodeGUID, client)
			ns.grpcConnectionsByURL.Store(nodeURL, conn)
			ns.raftService.AddNode(whoIsResp.NodeGUID)
			log.Debugln("found ", whoIsResp.NodeGUID)
			continue
		}
		nodeID := ns.raftNodesGUIDSbyURL[nodeURL]
		oldClientInterface, _ := ns.grpcNodeClientByGUID.Load(nodeID)
		oldClient := oldClientInterface.(raft.RaftNodeClient)
		whoIsResp, err := oldClient.WhoIsWho(context.Background(), &raft.WhoIs{
			NodeGUID: ns.nodeID,
		})
		if err != nil {
			log.Errorf(":(  %s is lost %s", nodeURL, err.Error())
			conn, loaded := ns.grpcConnectionsByURL.LoadAndDelete(nodeID)
			if loaded && conn != nil {
				defer conn.(*grpc.ClientConn).Close()
			}

			ns.deleteNode(nodeID)
			continue
		}
		if whoIsResp.NodeGUID != nodeID {
			log.Errorf(":( GUID mismatch %s expected %s actual %s", nodeURL, nodeID, whoIsResp.NodeGUID)
			conn, _ := ns.grpcConnectionsByURL.LoadAndDelete(nodeID)
			defer conn.(*grpc.ClientConn).Close()
			ns.deleteNode(nodeID)
			continue
		}
	}
}

func (ns *GRPCNodeServerImpl) deleteNode(nodeID string) {
	ns.grpcNodeClientByGUID.Delete(nodeID)
	ns.raftService.DeleteNode(nodeID)
	delete(ns.raftNodesGUIDSbyURL, nodeID)

	//TODO: Стримы постараться закрыть штатно перед удалением
	ns.grpcAppendEntriesResponseStreamsByGUID.Delete(nodeID)
	ns.grpcVoteResponseStreamsByGUID.Delete(nodeID)
}
