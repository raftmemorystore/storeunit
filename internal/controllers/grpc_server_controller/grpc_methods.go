package grpcservercontroller

import "gitlab.com/ferrysourcing/ferrybroker.git/pkg/api/raft"

func (ns *GRPCNodeServerImpl) RegisterAppendEntriesRequestStream(raft.RaftNode_RegisterAppendEntriesRequestStreamServer) error {
	return nil
}

func (ns *GRPCNodeServerImpl) RegisterAppendEntriesResponseStream(*raft.WhoIs, raft.RaftNode_RegisterAppendEntriesResponseStreamServer) error {
	return nil
}

func (ns *GRPCNodeServerImpl) RegisterRequestVoteRequestStream(raft.RaftNode_RegisterRequestVoteRequestStreamServer) error {
	return nil
}

func (ns *GRPCNodeServerImpl) RegisterRequestVoteResponseStream(*raft.WhoIs, raft.RaftNode_RegisterRequestVoteResponseStreamServer) error {
	return nil
}
