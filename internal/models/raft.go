package models

import (
	"sync"
	"time"

	publicmodel "gitlab.com/ferrysourcing/ferrybroker.git/pkg/models"
)

type AppendEntriesRequest struct {
	Term         int64
	LeaderId     string
	PrevLogIndex int64
	PrevLogTerm  int64
	LeaderCommit int64
	Entries      []*publicmodel.Entry
}

type RequestVoteRequest struct {
	Topic           string
	Term            int64
	CandidateNodeID string
	LastLogIndex    int64
	LastLogTerm     int64
}

type AppendEntriesResponse struct {
	NodeID  string
	Term    int64
	Success bool
}

type RequestVoteResponse struct {
	NodeID      string
	Term        int64
	VoteGranted bool
	Topic       string
}

type TopicNodeState struct {
	LastAppendEntryRespTimeStamp time.Time
}

type TopicState struct {
	Name              string
	NextIndex         int64
	MatchIndex        int64
	CurrentTerm       int64
	VotedFor          string
	CommitIndex       int64
	LastApplied       int64
	Votes             map[string]bool
	Role              NodeRole
	TermTimeStamp     time.Time
	ElectionTimeStamp time.Time
	NodeStates        sync.Map
}

type NodeRole int8

const (
	RAFT_ROLE_LEADER    NodeRole = 0
	RAFT_ROLE_CANDIDATE NodeRole = 1
	RAFT_ROLE_FOLLOWER  NodeRole = 2
)

func (role NodeRole) ToString() string {
	switch role {
	case RAFT_ROLE_LEADER:
		return "LEADER"
	case RAFT_ROLE_CANDIDATE:
		return "CANDIDATE"
	case RAFT_ROLE_FOLLOWER:
		return "FOLLOWER"
	}
	return "ERROR"
}
