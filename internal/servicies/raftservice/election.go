package raftservice

import (
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/ferrysourcing/ferrybroker.git/internal/models"
)

func (rs *RaftService) processVoteRespMessage(tpoicState *models.TopicState, voteResp *models.RequestVoteResponse) {

	if voteResp.Term > tpoicState.CurrentTerm {
		tpoicState.Role = models.RAFT_ROLE_FOLLOWER
		tpoicState.VotedFor = voteResp.NodeID
		tpoicState.TermTimeStamp = time.Now()
		log.Infof(":( I %s have become a follower for %s with term %d\n", rs.nodeId, voteResp.NodeID, tpoicState.CurrentTerm)
		return
	}

	if tpoicState.Role == models.RAFT_ROLE_CANDIDATE {
		tpoicState.Votes[voteResp.NodeID] = voteResp.VoteGranted
		voices := 0
		for _, voice := range tpoicState.Votes {
			if voice {
				voices++
			}
		}
		if voices > lenSyncMap(&rs.nodeList)/2.0 {
			log.Infof(":) I %s have become a leader with term %d\n", rs.nodeId, tpoicState.CurrentTerm)
			tpoicState.Role = models.RAFT_ROLE_LEADER
		}
	}
}

func (rs *RaftService) processVoteReqMessage(tpoicState *models.TopicState) {
}

func (rs *RaftService) remindElection() {

	//Инициируем процедуру голосоввания за меня

	rs.topicStates.Range(func(key, value interface{}) bool {
		topicState := value.(*models.TopicState)
		if time.Now().Second()-topicState.TermTimeStamp.Second() >= rs.timeOut {
			//Обнуляем или инициируем голосование за меня
			topicState.Votes = make(map[string]bool)
			topicState.TermTimeStamp = time.Now()
			topicState.CurrentTerm++
			topicState.Role = models.RAFT_ROLE_CANDIDATE
			topicState.Votes[rs.nodeId] = true
			rs.sendVoteReqToAll(topicState)
		} else if (time.Now().Second()-topicState.ElectionTimeStamp.Second() >= rs.timeOut) && (topicState.Role == models.RAFT_ROLE_CANDIDATE) {
			topicState.ElectionTimeStamp = time.Now()
			//Напоминаем, что за меня нужно проголосовать
			rs.sendVoteReqToAll(topicState)
		}

		return true
	})

}

func (rs *RaftService) sendVoteReqToAll(topicState *models.TopicState) {
	rs.nodeList.Range(func(key, value interface{}) bool {
		nodeID := key.(string)
		topicState.NodeStates.LoadOrStore(nodeID, initTopicNodeState())
		if !topicState.Votes[nodeID] {
			log.Infof("I am %s sending requesteVote to %s, with term %d\n", rs.nodeId, nodeID, topicState.CurrentTerm)
			rs.requestVote(nodeID, &models.RequestVoteRequest{
				Topic:           topicState.Name,
				Term:            topicState.CurrentTerm,
				CandidateNodeID: rs.nodeId,
				//TODO: Взять из репозитория
				LastLogIndex: 0,
				LastLogTerm:  0,
			})
		}
		return true
	})
}

func lenSyncMap(m *sync.Map) int {
	var i int
	m.Range(func(k, v interface{}) bool {
		i++
		return true
	})
	return i
}
