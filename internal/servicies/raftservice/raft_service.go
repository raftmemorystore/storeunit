package raftservice

import (
	"sync"
	"time"

	"gitlab.com/ferrysourcing/ferrybroker.git/internal/models"
)

type RaftService struct {
	nodeId        string
	appendEntries SendAToAllAppendEntriesFunc
	requestVote   SendRequestVoteFunc
	nodeList      sync.Map

	leaderAppendEntriesResponsesChan  chan *models.AppendEntriesResponse
	candidateRequestVoteResponsesChan chan *models.RequestVoteResponse

	// ParticipantRequestVoteRequestChan  chan *models.RequestVoteRequest
	// ParticipantRequestVoteResponseChan chan *models.RequestVoteResponse

	// FollowerAppendEntriesRequestChat  chan *models.AppendEntriesRequest
	// FollowerAppendEntriesResponseChan chan *models.AppendEntriesResponse

	timeOut int

	topicStates sync.Map
}

type SendAToAllAppendEntriesFunc func(map[string]*models.AppendEntriesRequest, chan *models.AppendEntriesResponse)
type SendRequestVoteFunc func(string, *models.RequestVoteRequest)

func NewRaftService(nodeId string, nodeQty int, appendEntriesFunc SendAToAllAppendEntriesFunc, requestVoteFunc SendRequestVoteFunc) *RaftService {

	raftService := RaftService{

		appendEntries:                     appendEntriesFunc,
		requestVote:                       requestVoteFunc,
		nodeId:                            nodeId,
		timeOut:                           3000,
		candidateRequestVoteResponsesChan: make(chan *models.RequestVoteResponse, nodeQty),
		leaderAppendEntriesResponsesChan:  make(chan *models.AppendEntriesResponse, nodeQty),
		// nodeStates:                        make(map[string]models.NodeState),
	}

	return &raftService
}

func (rs *RaftService) ExchangeProccessor() {
	for {

		var voteResp *models.RequestVoteResponse
		select {
		case voteResp = <-rs.candidateRequestVoteResponsesChan:
			if voteResp != nil {
				topicState, _ := rs.topicStates.LoadOrStore(voteResp.Topic, &models.TopicState{
					Name:              voteResp.Topic,
					NextIndex:         1,
					CurrentTerm:       0,
					MatchIndex:        0,
					VotedFor:          "",
					CommitIndex:       0,
					LastApplied:       0,
					Votes:             make(map[string]bool),
					Role:              models.RAFT_ROLE_FOLLOWER,
					TermTimeStamp:     time.Now(),
					ElectionTimeStamp: time.Now(),
				})
				rs.processVoteRespMessage(topicState.(*models.TopicState), voteResp)
			}
		default:
			rs.remindElection()
		}
	}
}

func (rs *RaftService) AddNode(nodeId string) {
	rs.nodeList.Store(nodeId, true)
}

func (rs *RaftService) DeleteNode(nodeId string) {
	rs.nodeList.Delete(nodeId)
}
